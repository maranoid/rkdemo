//
//  Pointer+CoreDataProperties.h
//  RKDemo
//
//  Created by Marat Abuiev on 3/18/16.
//  Copyright © 2016 Marat Abuiev. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Pointer.h"

NS_ASSUME_NONNULL_BEGIN

@interface Pointer (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *objectName;
@property (nullable, nonatomic, retain) NSString *type;
@property (nullable, nonatomic, retain) NSSet<NSManagedObject *> *event;
@property (nullable, nonatomic, retain) NSManagedObject *venue;

@end

@interface Pointer (CoreDataGeneratedAccessors)

- (void)addEventObject:(NSManagedObject *)value;
- (void)removeEventObject:(NSManagedObject *)value;
- (void)addEvent:(NSSet<NSManagedObject *> *)values;
- (void)removeEvent:(NSSet<NSManagedObject *> *)values;

@end

NS_ASSUME_NONNULL_END
