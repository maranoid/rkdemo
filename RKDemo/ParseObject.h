//
//  ParseObject.h
//  RKDemo
//
//  Created by Marat Abuiev on 1/21/16.
//  Copyright © 2016 Marat Abuiev. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ParseObject <NSObject>
- (NSString*)objectId;

@end
@interface ParseObject : NSManagedObject
@end
