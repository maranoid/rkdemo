//
//  Location+CoreDataProperties.m
//  RKDemo
//
//  Created by Marat Abuiev on 3/18/16.
//  Copyright © 2016 Marat Abuiev. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Location+CoreDataProperties.h"

@implementation Location (CoreDataProperties)

@dynamic latitude;
@dynamic longitude;
@dynamic type;
@dynamic venue;

- (NSString*)description
{
    return [NSString stringWithFormat:@"%li, %li", (long)self.latitude.integerValue, (long)self.longitude.integerValue];
}

@end
