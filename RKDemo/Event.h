//
//  Event.h
//  RKDemo
//
//  Created by Marat Abuiev on 3/18/16.
//  Copyright © 2016 Marat Abuiev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ParseObject+CoreDataProperties.h"

@interface Event : ParseObject

@end

#import "Event+CoreDataProperties.h"