//
//  ParseObject+CoreDataProperties.h
//  RKDemo
//
//  Created by Marat Abuiev on 3/18/16.
//  Copyright © 2016 Marat Abuiev. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ParseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface ParseObject (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *objectId;
@property (nullable, nonatomic, retain) NSDate *updatedAt;

@end

NS_ASSUME_NONNULL_END
