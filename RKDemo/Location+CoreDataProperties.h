//
//  Location+CoreDataProperties.h
//  RKDemo
//
//  Created by Marat Abuiev on 3/18/16.
//  Copyright © 2016 Marat Abuiev. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Location.h"

NS_ASSUME_NONNULL_BEGIN

@interface Location (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *latitude;
@property (nullable, nonatomic, retain) NSNumber *longitude;
@property (nullable, nonatomic, retain) NSString *type;
@property (nullable, nonatomic, retain) NSSet<NSManagedObject *> *venue;

@end

@interface Location (CoreDataGeneratedAccessors)

- (void)addVenueObject:(NSManagedObject *)value;
- (void)removeVenueObject:(NSManagedObject *)value;
- (void)addVenue:(NSSet<NSManagedObject *> *)values;
- (void)removeVenue:(NSSet<NSManagedObject *> *)values;

@end

NS_ASSUME_NONNULL_END
