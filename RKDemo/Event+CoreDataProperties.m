//
//  Event+CoreDataProperties.m
//  RKDemo
//
//  Created by Marat Abuiev on 3/18/16.
//  Copyright © 2016 Marat Abuiev. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Event+CoreDataProperties.h"
#import "Date+CoreDataProperties.h"
@implementation Event (CoreDataProperties)

@dynamic text;
@dynamic date;
@dynamic venue;

- (NSString*)description
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd', 'yyyy"];

    return [NSString stringWithFormat:@"%@: %@", [formatter stringFromDate:self.date.date], self.text];
}

@end
