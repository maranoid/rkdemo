//
//  Venue+CoreDataProperties.h
//  RKDemo
//
//  Created by Marat Abuiev on 3/18/16.
//  Copyright © 2016 Marat Abuiev. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Venue.h"
#import "Pointer+CoreDataProperties.h"

NS_ASSUME_NONNULL_BEGIN

@class Location;

@interface Venue (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *age;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) Pointer *pointer;
@property (nullable, nonatomic, retain) Location *location;

@end

NS_ASSUME_NONNULL_END
