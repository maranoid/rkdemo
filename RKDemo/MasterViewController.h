//
//  MasterViewController.h
//  RKDemo
//
//  Created by Marat Abuiev on 3/18/16.
//  Copyright (c) 2016 Marat Abuiev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MasterViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@end
