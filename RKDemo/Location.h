//
//  Location.h
//  RKDemo
//
//  Created by Marat Abuiev on 3/18/16.
//  Copyright (c) 2016 Marat Abuiev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Location : NSManagedObject
@end

#import "Location+CoreDataProperties.h"