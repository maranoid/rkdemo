//
//  Venue+CoreDataProperties.m
//  RKDemo
//
//  Created by Marat Abuiev on 3/18/16.
//  Copyright © 2016 Marat Abuiev. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Venue+CoreDataProperties.h"

@implementation Venue (CoreDataProperties)

@dynamic age;
@dynamic name;
@dynamic pointer;
@dynamic location;

- (NSString*)description
{
    return [NSString stringWithFormat:@"%p: id: %@ ", self, self.objectId];
}
@end
