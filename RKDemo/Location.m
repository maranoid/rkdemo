//
//  Location.m
//  RKDemo
//
//  Created by Marat Abuiev on 3/18/16.
//  Copyright (c) 2016 Marat Abuiev. All rights reserved.
//

#import "Location.h"

@implementation Location
- (void)awakeFromInsert
{
    [super awakeFromInsert];
    self.type = @"GeoPoint";
}
@end
