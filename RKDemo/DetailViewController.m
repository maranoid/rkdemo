//
//  DetailViewController.m
//  RKDemo
//
//  Created by Marat Abuiev on 3/18/16.
//  Copyright (c) 2016 Marat Abuiev. All rights reserved.
//

#import "DetailViewController.h"
#import "RestKit.h"
#import "Pointer.h"
#import "Event.h"
#import "Location.h"

@interface DetailViewController ()
- (void)configureView;
@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        [self loadEventsForVenue:_detailItem];
    }
}

- (void)configureView
{
    // Update the user interface for the detail item.

    if (self.detailItem) {
        self.detailDescriptionLabel.text = [self.detailItem name];
        self.ageLabel.text = [[self.detailItem age] description];
        self.locationLabel.text = [[self.detailItem location] description];
        // Fetch local data first
        [self updateEventsList:[self fetchEventsForVenue:self.detailItem]];
        
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadEventsForVenue:(Venue*)venue
{
    Pointer *pointer = venue.pointer;
    if(!pointer) {
        NSManagedObjectContext *context = [[[RKObjectManager sharedManager] managedObjectStore] mainQueueManagedObjectContext];
        pointer = [context insertNewObjectForEntityForName:@"Pointer"];
        [pointer setObjectName:NSStringFromClass(venue.class)];
        [pointer setObjectId:venue.objectId];
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.objectClass == %@", [Pointer class]];
    RKRequestDescriptor *requestDescriptor = [[[[RKObjectManager sharedManager] requestDescriptors] filteredArrayUsingPredicate:predicate] firstObject];
    
    NSError* error;
    NSDictionary *parameters = [RKObjectParameterization parametersWithObject:pointer
                                                            requestDescriptor:requestDescriptor
                                                                        error:&error];
    // Serialize the object to JSON
    NSData *JSON = [RKMIMETypeSerialization dataFromObject:parameters
                                                  MIMEType:[[RKObjectManager sharedManager] requestSerializationMIMEType]
                                                     error:&error];
    NSString *venuePointerJSON = [[NSString alloc] initWithData:JSON encoding:NSUTF8StringEncoding];
    
    
    [[RKObjectManager sharedManager] getObjectsAtPath:@"/1/classes/Event"
                                           parameters:@{@"where" : [NSString stringWithFormat: @"{\"venue\" : %@}", venuePointerJSON]}
                                              success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                  NSArray *events = mappingResult.array;
                                                  [self updateEventsList:events];
                                                  venue.pointer = [(Event *)events.firstObject venue];
                                                  [venue.managedObjectContext save:nil];
                                              }
                                              failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                  NSLog(@"No or malformed response: %@", error);
                                              }];
}

- (void)updateEventsList:(NSArray*)events
{
    NSMutableString *eventsText = [NSMutableString new];
    for(Event *e in events) {
        [eventsText appendFormat:@"%@\n", [e description]];
    }
    self.eventsLabel.text = eventsText.length ? eventsText : @"No events";
}

- (NSArray *)fetchEventsForVenue:(Venue *)venue
{
    NSManagedObjectContext *context = [[[RKObjectManager sharedManager] managedObjectStore] mainQueueManagedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Event" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.venue.objectId == %@", venue.objectId];
    [fetchRequest setPredicate:predicate];
    NSError *error;
    NSArray *events = [context executeFetchRequest:fetchRequest error:&error];
    if(error) {
        NSLog(@"Events fetch failed: %@", [error description]);
    }
    return events;
}
@end
