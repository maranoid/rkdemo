//
//  Pointer.h
//  RKDemo
//
//  Created by Marat Abuiev on 1/21/16.
//  Copyright © 2016 Marat Abuiev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ParseObject+CoreDataProperties.h"

@interface Pointer : ParseObject

@end

#import "Pointer+CoreDataProperties.h"