//
//  MasterViewController.m
//  RKDemo
//
//  Created by Marat Abuiev on 3/18/16.
//  Copyright (c) 2016 Marat Abuiev. All rights reserved.
//

#import "MasterViewController.h"
#import "RKPaginator.h"

#import "DetailViewController.h"
#import <RestKit/RestKit.h>
#import "Venue.h"
#import "Event.h"
#import "Pointer.h"
#import "Date.h"
#import "Location.h"
#import "DateValueTransformer.h"


#define kCLIENTID @"eViAXMWKlhyQaYbKDtKUcJEskSnhVtM7RkNM1jcv"
#define kCLIENTSECRET @"uCx9Lm8Tbr5w4VwbEVlTlf7yQxTq1HYh8lIfvJ4x"

@interface MasterViewController ()
@property (strong, nonatomic) RKManagedObjectStore *managedObjectStore;
@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
@end

@implementation MasterViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    self.navigationItem.rightBarButtonItem = addButton;

    [self configureRestKit];
    self.refreshControl = [UIRefreshControl new];
    [self.refreshControl addTarget:self action:@selector(loadVenues) forControlEvents:UIControlEventValueChanged];
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error]) {
        // Update to handle the error appropriately.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        exit(-1);  // Fail
    }
}

- (void)configureRestKit
{
    // initialize AFNetworking HTTPClient
    NSURL *baseURL = [NSURL URLWithString:@"https://api.parse.com"];
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    
    [client setDefaultHeader:@"X-Parse-Application-Id" value:kCLIENTID];
    [client setDefaultHeader:@"X-Parse-REST-API-Key" value:kCLIENTSECRET];
    
    // initialize RestKit
    RKObjectManager *objectManager = [[RKObjectManager alloc] initWithHTTPClient:client];
    
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
#pragma mark Serialization setup
    // Serialize to JSON
    objectManager.requestSerializationMIMEType = RKMIMETypeJSON;
    
    [self initializeStore];
 
#pragma mark Descriptors and mapping setup
    // setup object mappings
    RKEntityMapping *venueMapping = [RKEntityMapping mappingForEntityForName:@"Venue" inManagedObjectStore:self.managedObjectStore];
    [venueMapping addAttributeMappingsFromArray:@[@"name", @"age", @"objectId", @"updatedAt"]];
    venueMapping.identificationAttributes = @[@"objectId"];
    
    RKEntityMapping *locationMapping = [RKEntityMapping mappingForEntityForName:@"Location" inManagedObjectStore:self.managedObjectStore];
    [locationMapping addAttributeMappingsFromDictionary:@{@"latitude" : @"latitude", @"longitude" : @"longitude", @"__type" : @"type"}];
    
    [venueMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"location"
                                                                                   toKeyPath:@"location"
                                                                                 withMapping:locationMapping]];

    // register mappings with the provider using a response descriptor
    RKResponseDescriptor *responseDescriptor =
    [RKResponseDescriptor responseDescriptorWithMapping:venueMapping
                                                 method:RKRequestMethodGET
                                            pathPattern:@"/1/classes/Venue"
                                                keyPath:@"results"
                                            statusCodes:[NSIndexSet indexSetWithIndex:200]];
    
    [objectManager addResponseDescriptor:responseDescriptor];
    
    RKRequestDescriptor *requestDescriptor =
    [RKRequestDescriptor requestDescriptorWithMapping:[venueMapping inverseMapping]
                                          objectClass:[Venue class]
                                          rootKeyPath:nil
                                               method:RKRequestMethodAny];
    
    [objectManager addRequestDescriptor:requestDescriptor];
    
    RKEntityMapping *eventMapping = [RKEntityMapping mappingForEntityForName:@"Event" inManagedObjectStore:self.managedObjectStore];
    [eventMapping addAttributeMappingsFromDictionary:@{@"description" : @"text", @"objectId" : @"objectId"}];
    eventMapping.identificationAttributes = @[@"objectId"];
    
    RKEntityMapping *pointerMapping = [RKEntityMapping mappingForEntityForName:@"Pointer" inManagedObjectStore:self.managedObjectStore];
    [pointerMapping addAttributeMappingsFromDictionary:@{@"className" : @"objectName", @"objectId" : @"objectId", @"__type" : @"type"}];
    pointerMapping.identificationAttributes = @[@"objectId"];
    
    [eventMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"venue"
                                                                                 toKeyPath:@"venue"
                                                                               withMapping:pointerMapping]];
    
    requestDescriptor =
    [RKRequestDescriptor requestDescriptorWithMapping:[pointerMapping inverseMapping]
                                          objectClass:[Pointer class]
                                          rootKeyPath:nil
                                               method:RKRequestMethodGET];
    
    [objectManager addRequestDescriptor:requestDescriptor];
    

    RKEntityMapping *dateMapping = [RKEntityMapping mappingForEntityForName:@"Date" inManagedObjectStore:self.managedObjectStore];
    [dateMapping addAttributeMappingsFromDictionary:@{@"iso": @"isoString", @"__type" : @"type"}];
    [eventMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"date"
                                                                                 toKeyPath:@"date"
                                                                               withMapping:dateMapping]];
    // register mappings with the provider using a response descriptor
    responseDescriptor =
    [RKResponseDescriptor responseDescriptorWithMapping:eventMapping
                                                 method:RKRequestMethodGET
                                            pathPattern:@"/1/classes/Event"
                                                keyPath:@"results"
                                            statusCodes:[NSIndexSet indexSetWithIndex:200]];
    [objectManager addResponseDescriptor:responseDescriptor];
    
#pragma mark RKRouter setup
    responseDescriptor =
    [RKResponseDescriptor responseDescriptorWithMapping:venueMapping
                                                 method:RKRequestMethodGET
                                            pathPattern:@"/1/classes/Venue/:objectId"
                                                keyPath:nil
                                            statusCodes:[NSIndexSet indexSetWithIndex:200]];
    [objectManager addResponseDescriptor:responseDescriptor];
    RKRoute *myRoute = [RKRoute routeWithClass:[Venue class]
                                   pathPattern:@"/1/classes/Venue/:objectId"
                                        method:RKRequestMethodGET];
    [objectManager.router.routeSet addRoute:myRoute];
    responseDescriptor =
    [RKResponseDescriptor responseDescriptorWithMapping:venueMapping
                                                 method:RKRequestMethodDELETE
                                            pathPattern:@"/1/classes/Venue/:objectId"
                                                keyPath:nil
                                            statusCodes:[NSIndexSet indexSetWithIndex:200]];
    [objectManager addResponseDescriptor:responseDescriptor];
    myRoute = [RKRoute routeWithClass:[Venue class]
                                   pathPattern:@"/1/classes/Venue/:objectId"
                                        method:RKRequestMethodDELETE];
    [objectManager.router.routeSet addRoute:myRoute];
    
    responseDescriptor =
    [RKResponseDescriptor responseDescriptorWithMapping:venueMapping
                                                 method:RKRequestMethodPOST
                                            pathPattern:@"/1/classes/Venue"
                                                keyPath:nil
                                            statusCodes:[NSIndexSet indexSetWithIndex:201]];
    [objectManager addResponseDescriptor:responseDescriptor];
    
    myRoute = [RKRoute routeWithClass:[Venue class]
                          pathPattern:@"/1/classes/Venue"
                               method:RKRequestMethodPOST];
    [objectManager.router.routeSet addRoute:myRoute];

    // Import seed data
    [self importObjectsFromFileURL:[[NSBundle mainBundle] URLForResource:@"venues" withExtension:@"json"]
                           withMapping:venueMapping];

#pragma mark Logging setup

    //RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
    //RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelTrace);
#pragma mark Value transformers
    RKCompoundValueTransformer *valueTransformer = [RKValueTransformer defaultValueTransformer];
    
    [valueTransformer removeValueTransformer:[RKValueTransformer timeIntervalSince1970ToDateValueTransformer]];
    [valueTransformer addValueTransformer:[DateValueTransformer new]];
    
#pragma end
}

- (void) initializeStore
{
    NSManagedObjectModel *managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    self.managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    
    NSString *storePath = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"rkdemo.sqlite"];
    NSError *error;
    NSPersistentStore *persistentStore = [self.managedObjectStore addSQLitePersistentStoreAtPath:storePath fromSeedDatabaseAtPath:nil withConfiguration:nil options:nil error:&error];
    NSAssert(persistentStore, @"Failed to add persistent store with error: %@", error);
    
    // Create the managed object contexts
    [self.managedObjectStore createManagedObjectContexts];
    
    // Configure a managed object cache to ensure we do not create duplicate objects
    self.managedObjectStore.managedObjectCache = [[RKInMemoryManagedObjectCache alloc] initWithManagedObjectContext:self.managedObjectStore.persistentStoreManagedObjectContext];
    [[RKObjectManager sharedManager] setManagedObjectStore:self.managedObjectStore];
}

- (void)saveData
{
    [self.managedObjectStore.mainQueueManagedObjectContext saveToPersistentStore:nil];
}

- (void)loadVenues
{
    [self syncLocalModels];
    
    [[RKObjectManager sharedManager] getObjectsAtPath:@"/1/classes/Venue"
                                           parameters:nil
                                              success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                  [self removeRemotelyDeletedItems:mappingResult.array];
                                                  [self.refreshControl endRefreshing];
                                              }
                                              failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                  NSLog(@"No or malformed response: %@", error);
                                                  [self.refreshControl endRefreshing];
                                              }];
}

- (void)importObjectsFromFileURL:(NSURL *)fileURL withMapping:(RKEntityMapping *)mapping
{
    NSParameterAssert(fileURL);
    
    RKManagedObjectImporter *importer = [[RKManagedObjectImporter alloc] initWithPersistentStore:self.managedObjectStore.persistentStoreCoordinator.persistentStores.firstObject];
    
    NSError *error;
    [importer importObjectsFromItemAtPath:[fileURL path]
                              withMapping:mapping
                                  keyPath:@"results"
                                    error:&error];
    
    BOOL success = [importer finishImporting:&error];
    if (!success) {
        NSLog(@"Import error: %@", error);
    }
}

- (void) syncLocalModels
{
    // Sync insertions
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Venue" inManagedObjectContext:self.managedObjectStore.mainQueueManagedObjectContext];
    [fetchRequest setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.objectId == NULL"];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *venues = [self.managedObjectStore.mainQueueManagedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    dispatch_group_t syncGroup = dispatch_group_create();
    
    dispatch_queue_t syncQueue = dispatch_queue_create("sync", DISPATCH_QUEUE_CONCURRENT);
    
    for(Venue *venue in venues) {
        dispatch_async(syncQueue, ^{
            dispatch_group_enter(syncGroup);
            [[RKObjectManager sharedManager] postObject:venue
                                                   path:nil
                                             parameters:nil
                                                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                    dispatch_group_leave(syncGroup);
                                                }
                                                failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                    dispatch_group_leave(syncGroup);
                                                }];

        });
    }
    dispatch_group_wait(syncGroup, DISPATCH_TIME_FOREVER);
}

- (void)removeRemotelyDeletedItems:(NSArray*)remoteVenues
{
    NSArray *objectIds = [remoteVenues valueForKeyPath:@"@distinctUnionOfObjects.objectId"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"NOT (self.objectId IN %@) AND self.objectId != NULL", objectIds];
    NSArray *locals = [[self.fetchedResultsController fetchedObjects] filteredArrayUsingPredicate:predicate];
    for (Venue *venue in locals) {
        [venue.managedObjectContext deleteObject:venue];
    }
    [self saveData];
}

- (void)showError:(NSError *)error
{
    UIAlertView *theAlert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                       message:error.localizedDescription
                                                      delegate:self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil];
    [theAlert show];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id  sectionInfo =
    [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)insertNewObject:(id)sender
{
    NSManagedObjectContext *context = self.managedObjectStore.mainQueueManagedObjectContext;
    Venue *venue = [context insertNewObjectForEntityForName:@"Venue"];
    
    Location *loc = [context insertNewObjectForEntityForName:@"Location"];
    [loc setLatitude:@(34)];
    [loc setLongitude:@(46)];
    
    [venue setName:[NSString stringWithFormat:@"Venue_%f", [[NSDate date] timeIntervalSince1970]]];
    [venue setLocation:loc];
    [venue setAge:@(0)];
    
    [[RKObjectManager sharedManager] postObject:venue
                                           path:nil
                                     parameters:nil
                                        success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                        }
                                        failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                            [self saveData];
                                            [self showError:error];
                                        }];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Venue *venue =[self.fetchedResultsController objectAtIndexPath:indexPath];
        
#pragma mark RKRouter usage
        [[RKObjectManager sharedManager] deleteObject:venue
                                                 path:nil
                                           parameters:nil
                                              success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                              }
                                              failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                  
                                                  if(operation.HTTPRequestOperation.response.statusCode == 404) {
                                                      [venue.managedObjectContext deleteObject:venue];
                                                      [self saveData];
                                                  }
                                                  [self showError:error];
                                              }];
#pragma end
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Venue *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
#pragma mark RKRouter usage
        [[RKObjectManager sharedManager] getObject:object path:nil parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
            NSLog(@"getObject: %@", [object description]);
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            NSLog(@"No or malformed response: %@", error);
        }];
#pragma end
        [[segue destinationViewController] setDetailItem:object];
    }
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Venue *object =[self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = [object name];
    if(!object.objectId) {
        cell.userInteractionEnabled = NO;
        [cell.contentView setAlpha:0.3];
    } else {
        cell.userInteractionEnabled = YES;
        [cell.contentView setAlpha:1.0];
    }
}

#pragma mark - fetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController {
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Venue" inManagedObjectContext:self.managedObjectStore.mainQueueManagedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc]
                              initWithKey:@"age" ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    
    [fetchRequest setFetchBatchSize:20];
    
    self.fetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:self.managedObjectStore.mainQueueManagedObjectContext sectionNameKeyPath:nil
                                                   cacheName:@"Root"];
    self.fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
    
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.tableView endUpdates];
}
@end
