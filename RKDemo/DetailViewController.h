//
//  DetailViewController.h
//  RKDemo
//
//  Created by Marat Abuiev on 3/18/16.
//  Copyright (c) 2016 Marat Abuiev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Venue.h"

@interface DetailViewController : UIViewController

@property (strong, nonatomic) Venue * detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UITextView *eventsLabel;
@end

