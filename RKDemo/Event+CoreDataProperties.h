//
//  Event+CoreDataProperties.h
//  RKDemo
//
//  Created by Marat Abuiev on 3/18/16.
//  Copyright © 2016 Marat Abuiev. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Event.h"
#import "Pointer+CoreDataProperties.h"

NS_ASSUME_NONNULL_BEGIN

@class Date;

@interface Event (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *text;
@property (nullable, nonatomic, retain) Date *date;
@property (nullable, nonatomic, retain) Pointer *venue;

@end

NS_ASSUME_NONNULL_END
