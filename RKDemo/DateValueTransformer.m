//
//  DateValueTransformer.m
//  RKDemo
//
//  Created by Marat Abuyev on 3/18/16.
//  Copyright © 2016 Marat Abuiev. All rights reserved.
//

#import "DateValueTransformer.h"

@implementation DateValueTransformer
- (BOOL)validateTransformationFromClass:(Class)inputValueClass toClass:(Class)outputValueClass
{
    return (([inputValueClass isSubclassOfClass:[NSNumber class]] && [outputValueClass isSubclassOfClass:[NSDate class]]) ||
            ([inputValueClass isSubclassOfClass:[NSDate class]] && [outputValueClass isSubclassOfClass:[NSNumber class]]));
}

- (BOOL)transformValue:(id)inputValue toValue:(id *)outputValue ofClass:(Class)outputValueClass error:(NSError **)error
{
    RKValueTransformerTestInputValueIsKindOfClass(inputValue, (@[ [NSNumber class], [NSDate class] ]), error);
    RKValueTransformerTestOutputValueClassIsSubclassOfClass(outputValueClass, (@[ [NSNumber class], [NSDate class] ]), error);
    if ([inputValue isKindOfClass:[NSNumber class]]) {
        NSString *errorDescription = nil;
        BOOL success = [self getObjectValue:outputValue forInterval:inputValue errorDescription:&errorDescription];
        RKValueTransformerTestTransformation(success, error, @"%@", errorDescription);
    } else if ([inputValue isKindOfClass:[NSDate class]]) {
        NSDateFormatter *dateFormat = [NSDateFormatter new];
        [dateFormat setDateStyle:NSDateFormatterMediumStyle];
        *outputValue = [dateFormat stringFromDate:inputValue];
    }
    return YES;
}

- (BOOL)getObjectValue:(id *)outValue forInterval:(NSNumber *)interval errorDescription:(NSString **)error
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval.integerValue/1000];
    if (outValue)
        *outValue = date;
    return (date != nil);
}

@end
