//
//  Pointer+CoreDataProperties.m
//  RKDemo
//
//  Created by Marat Abuiev on 3/18/16.
//  Copyright © 2016 Marat Abuiev. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Pointer+CoreDataProperties.h"

@implementation Pointer (CoreDataProperties)

@dynamic objectName;
@dynamic type;
@dynamic event;
@dynamic venue;

@end
