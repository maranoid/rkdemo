//
//  main.m
//  RKDemo
//
//  Created by Marat Abuiev on 3/18/16.
//  Copyright (c) 2016 Marat Abuiev. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
