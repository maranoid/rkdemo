//
//  Date+CoreDataProperties.m
//  RKDemo
//
//  Created by Marat Abuiev on 3/18/16.
//  Copyright © 2016 Marat Abuiev. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Date+CoreDataProperties.h"

@implementation Date (CoreDataProperties)

@dynamic isoString;
@dynamic type;
@dynamic event;

- (NSDate*)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSS'Z'"];
    return [formatter dateFromString:self.isoString];
}

@end
